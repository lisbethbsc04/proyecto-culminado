import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/registro',
    name: 'registro',
    component: () => import(/* webpackChunkName: "registrarse" */ '../views/Registro.vue')
  },
  {
    path: '/articulos',
    name: 'articulos',
    component: () => import(/* webpackChunkName: "horarios" */ '../views/Articulos.vue')
  },
  {
    path: '/contactos',
    name: 'contactos',
    component: () => import(/* webpackChunkName: "horarios" */ '../views/Contactos.vue')
  },
  {
    path: '/datos',
    name: 'datos',
    component: () => import(/* webpackChunkName: "horarios" */ '../views/Datos.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router